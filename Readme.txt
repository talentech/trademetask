
This app displays all categories and first 20 listings of each category. For simplicity, listings
details Activity is not implemented.

It has only one MainActivity, with two ListFragments in the Activity. CategoryFragment lives in a
drawer, or side by side with ListingFragments, depending on screen width. Screen-rotation is
supported.

android.content.Loader is used to load data.

3rd open source libraries used:

1. Dagger 2  (http://google.github.io/dagger/)
dependency injection

2. Otto      (http://square.github.io/otto/)
event bus, provides decoupled communication between the 2 fragments.

3. Retrofit  (http://square.github.io/retrofit/)
type-safe REST service interface creator

4. Picasso   (http://square.github.io/picasso/)
image loading/catching

5. Signpost  (https://github.com/mttkay/signpost)
OAuth 1.0 signing



