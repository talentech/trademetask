package nz.co.trademe.trademetask.loader;

import java.util.Map;

import javax.inject.Inject;

import nz.co.trademe.trademetask.model.Listing;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Fetcher factory to get fetcher for listing
 */
public class ListingFetcherFactory extends RestFetcherFactoryImpl<Listing> {

    private final RestService restService;

    @Inject
    public ListingFetcherFactory() {
        restService = ServiceGenerator.createService(RestService.class);
    }

    private interface RestService {
        @GET("/v1/Search/General.json")
        Call<Listing> get(@QueryMap Map<String, String> options);
    }

    @Override
    Call<Listing> getRestCall(Map<String, String> urlParams) {
        if (urlParams != null)
            return restService.get(urlParams);
        else
            return null;
    }
}
