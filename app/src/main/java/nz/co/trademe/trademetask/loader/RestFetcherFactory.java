package nz.co.trademe.trademetask.loader;

import java.util.Map;

import nz.co.trademe.trademetask.model.ModelBase;

/**
 * Factory of type-safe fetcher
 */
public interface RestFetcherFactory<D extends ModelBase> {

    /**
     * Gets a type-safe fetcher
     *
     * @param urlParams parameters of Url, may be headers, query strings, etc.
     * @return type-safe fetcher
     */
    RestFetcher<D> get(final Map<String, String> urlParams);
}
