package nz.co.trademe.trademetask.loader;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import nz.co.trademe.trademetask.model.Category;
import retrofit.Call;
import retrofit.http.GET;

/**
 * Fetcher factory to get fetcher for category
 */
@Singleton
public class CategoryFetcherFactory extends RestFetcherFactoryImpl<Category> {

    private final RestService restService;

    @Inject
    public CategoryFetcherFactory() {
        restService = ServiceGenerator.createService(RestService.class);
    }

    private interface RestService {
        @GET("/v1/Categories.json")
        Call<Category> get();
    }

    @Override
    Call<Category> getRestCall(Map<String, String> urlParams) {
        return restService.get();
    }
}
