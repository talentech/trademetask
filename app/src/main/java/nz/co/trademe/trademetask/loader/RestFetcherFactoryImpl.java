package nz.co.trademe.trademetask.loader;

import java.io.IOException;
import java.util.Map;

import nz.co.trademe.trademetask.model.ModelBase;
import retrofit.Call;
import retrofit.Response;

/**
 * Implementation of type-safe fetcher factory
 */
public abstract class RestFetcherFactoryImpl<D extends ModelBase> implements RestFetcherFactory<D> {

    // to be overridden by subclasses to get Retrofit Call object
    abstract Call<D> getRestCall(Map<String, String> urlParams);

    @Override
    public RestFetcher<D> get(final Map<String, String> urlParams) {
        final Call<D> categoryCall = getRestCall(urlParams);

        if (categoryCall == null)
            return null;

        // create fetcher
        return new RestFetcher<D>() {

            @Override
            public RestResponse<D> fetch() {
                Response<D> response = null;
                try {
                    response = categoryCall.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    String error = response.errorBody().string();
                    error.length();
                } catch (Exception e) {
                    //e.printStackTrace();
                }

                RestResponse<D> restResponse = new RestResponse<>();

                if (response != null) {
                    D data = response.body();
                    if (data != null) {
                        restResponse.setData(data);
                    }
                    else {
                        try {
                            restResponse.setError(response.errorBody().string());
                        } catch (Exception e) {
                            restResponse.setError("Unknown error");
                        }
                    }
                }
                else
                    restResponse.setError("Exception occurred");

                return restResponse;
            }

            @Override
            public void cancel() {
                categoryCall.cancel();
            }
        };


    }

}
