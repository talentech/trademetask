package nz.co.trademe.trademetask;

import android.app.Application;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nz.co.trademe.trademetask.loader.CategoryFetcherFactory;
import nz.co.trademe.trademetask.loader.ListingFetcherFactory;
import nz.co.trademe.trademetask.loader.RestLoader;
import nz.co.trademe.trademetask.model.Category;
import nz.co.trademe.trademetask.model.Listing;

/**
 * Dependency provider.
 */
@Module
public class TradeMeTaskModule {

    private Application app;

    public TradeMeTaskModule(Application app) {
        this.app = app;
    }

    @Provides
    Application provideApplication() {
        return app;
    }

    @Provides @Singleton
    Bus provideBus() {
        return new Bus();
    }

    @Provides
    RestLoader<Category> provideCategoryLoader(CategoryFetcherFactory categoryFetcherFactory) {
        return new RestLoader<>(app, categoryFetcherFactory);
    }

    @Provides
    RestLoader<Listing> provideListingLoader(ListingFetcherFactory listingFetcherFactory) {
        return new RestLoader<>(app, listingFetcherFactory);
    }

}
