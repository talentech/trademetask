package nz.co.trademe.trademetask.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.Map;

import nz.co.trademe.trademetask.model.ModelBase;


/**
 * Loader that loads data from REST services
 * For each REST service, define a concrete subclass of RestFetcherFactoryImpl
 * See CategoryFetcherFactory and ListingFetcherFactory as examples
 */
public class RestLoader<D extends ModelBase> extends AsyncTaskLoader<RestResponse<D>> {

    private static final String TAG = "TradeMeTask";
    private static final boolean DEBUG = false;
    //private boolean DEBUG = true;

    // We hold a reference to the Loader's data here.
    private RestResponse<D> restResponse;
    private RestFetcherFactory<D> fetcherFactory;

    private Map<String, String> urlParams;

    private RestFetcher<D> fetcher;

    private boolean autoFirstLoading = true;

    // if data != null and a load is cancelled, then the loader is stopped and started again,
    // onStartLoading should do force load
    // at this time takeContentChanged() == false so this boolean flag is added to deal with this
    // scenario
    private boolean loadPending = false;

    public RestLoader(Context context, RestFetcherFactory<D> fetcherFactory) {
        super(context);

        this.fetcherFactory = fetcherFactory;

    }

    public void setUrlParams(Map<String, String> urlParams) {
        this.urlParams = urlParams;
    }

    @Override
    public RestResponse<D> loadInBackground() {
        if (DEBUG) Log.i(TAG, "*** loadInBackground() called! ***");

        fetcher = fetcherFactory.get(urlParams);


        if (fetcher != null) {
/*
            // simulate large network latency
            if (DEBUG)
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
*/
            return fetcher.fetch();
        }
        else
            return null;
    }


    @Override
    public void deliverResult(RestResponse<D> data) {
        if (isReset()) {
            if (DEBUG) Log.w(TAG, "*** Warning! An async query came in while the Loader was reset! ***");
            if (data != null) {
                releaseResources(data);
                return;
            }
        }

        loadPending = false;

        // Hold a reference to the old data so it doesn't get garbage collected.
        // We must protect it until the new data has been delivered.
        RestResponse<D> oldData = this.restResponse;
        this.restResponse = data;

        if (isStarted()) {
            if (DEBUG) Log.i(TAG, "*** Delivering results to the LoaderManager for" +
                    " the UI to display! ***");
            // If the Loader is in a started state, have the superclass deliver the
            // results to the client.
            super.deliverResult(data);
        }

        // Invalidate the old data as we don't need it any more.
        if (oldData != null && oldData != data) {
            if (DEBUG) Log.i(TAG, "*** Releasing any old data associated with this Loader. ***");
            releaseResources(oldData);
        }
    }

    @Override
    protected void onStartLoading() {
        if (DEBUG) Log.i(TAG, "*** onStartLoading() called! ***");

        boolean oldLoadPending = loadPending;

        if (restResponse != null) {
            // Deliver any previously loaded data immediately.
            if (DEBUG) Log.i(TAG, "*** Delivering previously loaded data to the client...");
            deliverResult(restResponse);
        }

        if (takeContentChanged()) {
            if (DEBUG) Log.i(TAG, "*** A content change has been detected... so force load! ***");
            forceLoad();
        } else if (restResponse == null) {
            // If the current data is null... then we should make it non-null! :)
            if (DEBUG) Log.i(TAG, "*** The current data is data is null... so force load! ***");
            forceLoad();
        } else if (oldLoadPending) {
            // If the last load is not finished (i.e., cancelled), do it again
            if (DEBUG) Log.i(TAG, "*** The last load is not finished... so force load! ***");
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        if (DEBUG) Log.i(TAG, "*** onStopLoading() called! ***");

        // The Loader has been put in a stopped state, so we should attempt to
        // cancel the current load (if there is one).
        cancelLoad();
    }

    @Override
    protected void onReset() {
        if (DEBUG) Log.i(TAG, "*** onReset() called! ***");

        // Ensure the loader is stopped.
        onStopLoading();

        // At this point we can release the resources associated with 'apps'.
        if (restResponse != null) {
            releaseResources(restResponse);
            restResponse = null;
        }

    }

    @Override
    public void onCanceled(RestResponse<D> data) {
        if (DEBUG) Log.i(TAG, "*** onCanceled() called! ***");

        // Attempt to cancel the current asynchronous load.
        super.onCanceled(data);

        // The load has been canceled, so we should release the resources
        // associated with 'data'.
        releaseResources(data);
    }

    @Override
    public void forceLoad() {
        if (DEBUG) Log.i(TAG, "*** forceLoad() called! ***");
        loadPending = true;
        super.forceLoad();
    }


    @Override
    protected boolean onCancelLoad() {
        if (DEBUG) Log.i(TAG, "*** onCancelLoad() called! ***");

        if (fetcher != null)
            fetcher.cancel();

        return super.onCancelLoad();
    }

    /**
     * Helper method to take care of releasing resources associated with an
     * actively loaded data set.
     */
    private void releaseResources(RestResponse<D> data) {
        // For a simple List, there is nothing to do. For something like a Cursor,
        // we would close it in this method. All resources associated with the
        // Loader should be released here.
    }
}
