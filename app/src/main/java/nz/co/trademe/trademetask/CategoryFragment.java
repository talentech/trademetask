package nz.co.trademe.trademetask;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.squareup.otto.Bus;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Provider;

import nz.co.trademe.trademetask.adapter.CategoryAdapter;
import nz.co.trademe.trademetask.adapter.CategoryEntry;
import nz.co.trademe.trademetask.adapter.CategoryStack;
import nz.co.trademe.trademetask.event.SelectCategoryEvent;
import nz.co.trademe.trademetask.loader.RestLoader;
import nz.co.trademe.trademetask.model.Category;
import nz.co.trademe.trademetask.loader.RestResponse;

/**
 * CategoryFragment displays category list.
 * Because subcategories may be in arbitrary depth, the displayed list is in a stack-like mode:
 * the upper part are parent categories starting from root, while the lower part are subcategories.
 * This is somewhat a combination of a breadcrumb (which is deprecated in SDK21) and a list.
 */
public class CategoryFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<RestResponse<Category>> {

    private static final String TAG = "TradeMeTask";
    private static final boolean DEBUG = false;

    private static final String KEY_CATEGORY_NUMBERS = "KEY_CATEGORY_NUMBERS";

    // The Loader's id (this id is specific to the Activity/Fragment's LoaderManager)
    private static final int LOADER_ID = 1;

    // loader used to load category data from server
    @Inject
    Provider<RestLoader<Category>> categoryLoader;

    // Otto event bus
    @Inject
    Bus bus;

    // a custom ArrayAdapter to bind category data to the ListView.
    private CategoryAdapter adapter;

    // a data structure that manages the stack-like category list
    private CategoryStack categoryStack;

    // number list from root to current subcategory number, used to save state of fragment
    // root number -> category number -> subcategory number -> ... -> current subcategory number
    private ArrayList<String> categoryNumbers;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null)
            categoryNumbers = savedInstanceState.getStringArrayList(KEY_CATEGORY_NUMBERS);

        Injector.component().inject(this);

        adapter = new CategoryAdapter(getActivity());
        setListAdapter(adapter);
        setEmptyText(getActivity().getString(R.string.categories_load_error));
        setListShown(false);

        // Initialize a Loader with id '1'. If the Loader with this id already
        // exists, then the LoaderManager will reuse the existing Loader.
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        if (categoryStack != null)
            outState.putStringArrayList(KEY_CATEGORY_NUMBERS, categoryStack.getCategoryNumbers());

        super.onSaveInstanceState(outState);
    }


    @Override
    public Loader<RestResponse<Category>> onCreateLoader(int id, Bundle args) {
        if (DEBUG) Log.i(TAG, "+++ onCreateLoader() called! +++");

        return categoryLoader.get();
    }

    @Override
    public void onLoadFinished(Loader<RestResponse<Category>> loader,
                               RestResponse<Category> restResponse) {
        if (DEBUG) Log.i(TAG, "+++ onLoadFinished() called! +++");

        if (restResponse != null && restResponse.isSuccess()) {
            Category data = restResponse.getData();

            // set root category name because it is "Root", not suit for display
            data.setName(getString(R.string.root_category_name));

            // create a CategoryStack to manage the stack-like category list
            categoryStack = new CategoryStack(data, categoryNumbers);

            // display categories in the stack
            adapter.setData(categoryStack.getCateoryEntries());
        }
        else
            // no data, error message will be shown
            adapter.setData(null);

        if (isResumed())
            setListShown(true);
        else
            setListShownNoAnimation(true);
    }

    @Override
    public void onLoaderReset(Loader<RestResponse<Category>> loader) {
        if (DEBUG) Log.i(TAG, "+++ onLoadReset() called! +++");

        adapter.setData(null);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        CategoryEntry item = (CategoryEntry) l.getItemAtPosition(position);

        if (item.isCurrent())
            return;

        // change current selected subcategory
        categoryStack.setCurrentCategory(item.getNumber());
        adapter.setData(categoryStack.getCateoryEntries());

        // notify other UI controllers to update their UI
        bus.post(new SelectCategoryEvent(item.getNumber(), item.isRoot()));
    }
}
