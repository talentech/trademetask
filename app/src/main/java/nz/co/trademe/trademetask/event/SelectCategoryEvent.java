package nz.co.trademe.trademetask.event;

/**
 * Otto event object that represents a selected category
 */
public class SelectCategoryEvent {

    private String categoryNumber;
    private boolean isRoot;

    public SelectCategoryEvent(String categoryNumber, boolean isRoot) {
        this.categoryNumber = categoryNumber;
        this.isRoot = isRoot;
    }

    public String getCategoryNumber() {
        return categoryNumber;
    }

    public boolean isRoot() {
        return isRoot;
    }
}
