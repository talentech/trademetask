package nz.co.trademe.trademetask.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import nz.co.trademe.trademetask.R;
import nz.co.trademe.trademetask.model.Listing;

/**
 * Adapter for Listing list
 */
public class ListingAdapter extends ArrayAdapter<Listing.Item> {

    private final LayoutInflater mInflater;
    private final Context context;

    public ListingAdapter(Context context) {
        super(context, 0);

        this.context = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public void setData(List<Listing.Item> listingItems) {
        clear();
        if (listingItems != null)
            addAll(listingItems);
    }

    static class ViewHolder {
        ImageView listingPicture;
        TextView listingTitle;
        TextView listingId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_listing, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.listingPicture = (ImageView) view.findViewById(R.id.listing_picture);
            viewHolder.listingTitle = (TextView) view.findViewById(R.id.listing_title);
            viewHolder.listingId = (TextView) view.findViewById(R.id.listing_id);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        Listing.Item item = getItem(position);
        Picasso.with(context).load(item.getPictureHref()).into(viewHolder.listingPicture);
        viewHolder.listingTitle.setText(item.getTitle());
        viewHolder.listingId.setText(item.getId());
        return view;
    }
}

