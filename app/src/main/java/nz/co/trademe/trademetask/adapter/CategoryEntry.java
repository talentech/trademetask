package nz.co.trademe.trademetask.adapter;

/**
 * Category info suitable for display
 */
public class CategoryEntry {

    public enum Type {
        ROOT,
        PARENT,
        CHILD
    }

    private String name;
    private String number;
    private Type type;
    private boolean isCurrent;
    private boolean hasSubcategories;

    public CategoryEntry(String name,
                         String number,
                         Type type,
                         boolean isCurrent,
                         boolean hasSubcategories) {
        this.name = name;
        this.number = number;
        this.type = type;
        this.isCurrent = isCurrent;
        this.hasSubcategories = hasSubcategories;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public Type getType() {
        return type;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public boolean hasSubcategories() {
        return hasSubcategories;
    }

    public boolean isRoot() {
        return type == Type.ROOT;
    }

    @Override
    public String toString() {
        return name;
    }
}
