package nz.co.trademe.trademetask.loader;

import com.squareup.okhttp.OkHttpClient;

import nz.co.trademe.trademetask.config.Config;
import retrofit.Retrofit;
import retrofit.MoshiConverterFactory;
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer;
import se.akerfeldt.okhttp.signpost.SigningInterceptor;

/**
 * Utility class to create Retrofit service objects
 */
class ServiceGenerator {
    // No need to instantiate this class.
    private ServiceGenerator() {
    }

    private static Retrofit retrofit;

    static {
        OkHttpOAuthConsumer consumer = new OkHttpOAuthConsumer(Config.OAUTH_CONSUMER_KEY,
                Config.OAUTH_CONSUMER_SECRET);

        //consumer.setTokenWithSecret(token, secret);

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(new SigningInterceptor(consumer));

        retrofit = new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .baseUrl(Config.SERVER_BASE_URL)
                .build();
    }

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
