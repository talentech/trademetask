package nz.co.trademe.trademetask.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.List;

import nz.co.trademe.trademetask.R;


/**
 * Adapter for category list
 */
public class CategoryAdapter extends ArrayAdapter<CategoryEntry> {

    private final LayoutInflater mInflater;
    private final int rootCategoryColor;
    private final int parentCategoryColor;

    public CategoryAdapter(Context context) {
        super(context, 0);

        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        rootCategoryColor = context.getResources().getColor(R.color.category_root);
        parentCategoryColor = context.getResources().getColor(R.color.category_parent);
    }

    public void setData(List<CategoryEntry> categoryEntries) {
        clear();
        if (categoryEntries != null)
            addAll(categoryEntries);
    }

    static class ViewHolder {
        TextView categoryName;
        TextView mark;

        Drawable background;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_category, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.categoryName = (TextView) view.findViewById(R.id.category_name);
            viewHolder.mark = (TextView) view.findViewById(R.id.mark);
            viewHolder.background = view.getBackground();
            view.setTag(viewHolder);
        }
        else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        CategoryEntry item = getItem(position);
        viewHolder.categoryName.setText(item.getName());

        // current category displayed in bold font
        if (item.isCurrent())
            viewHolder.categoryName.setTypeface(null, Typeface.BOLD);
        else
            viewHolder.categoryName.setTypeface(null, Typeface.NORMAL);

        // root and parent categories displayed in darker colors
        switch (item.getType()) {
            case ROOT:
                view.setBackgroundColor(rootCategoryColor);
                break;

            case PARENT:
                view.setBackgroundColor(parentCategoryColor);
                break;

            default:
                view.setBackgroundDrawable(viewHolder.background);
                break;
        }

        // show ">" if the category has subcategories
        if (!item.hasSubcategories()
                || item.getType() == CategoryEntry.Type.ROOT
                || item.getType() == CategoryEntry.Type.PARENT)
            viewHolder.mark.setVisibility(View.GONE);

        return view;
    }
}
