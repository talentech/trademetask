package nz.co.trademe.trademetask.config;

/**
 * Configuration info.
 */
public class Config {

    public static final String SERVER_BASE_URL = "https://api.tmsandbox.co.nz";
    public static final String OAUTH_CONSUMER_KEY = "D6DD525D93F54DD3E3AC18358DC39FB7";
    public static final String OAUTH_CONSUMER_SECRET = "9BAD3F5B8289224676FC9312F7100943";
    public static final String FETCH_LISTING_ROWS = "20";

}
