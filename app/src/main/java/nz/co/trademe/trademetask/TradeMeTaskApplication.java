package nz.co.trademe.trademetask;

import android.app.Application;

/**
 * Custom Application class to init the dependency injector
 */
public class TradeMeTaskApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // init Dagger component
        Injector.tradeMeTaskComponent = DaggerTradeMeTaskComponent.builder()
                .tradeMeTaskModule(new TradeMeTaskModule(this))
                .build();
    }
}
