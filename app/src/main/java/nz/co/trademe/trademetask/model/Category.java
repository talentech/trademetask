package nz.co.trademe.trademetask.model;

import java.util.List;

/**
 * Category tree.
 * Will be created by JSON converter.
 */
public class Category extends ModelBase {

    private String Name;
    private String Number;

    private List<Category> Subcategories;


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return Number;
    }

    public List<Category> getSubcategories() {
        return Subcategories;
    }

    public boolean hasSubcategories() {
        return Subcategories != null && Subcategories.size() > 0;
    }

    public Category getSubcategory(String number) {
        if (Subcategories != null) {
            for (Category category : Subcategories) {
                if (category.getNumber().equals(number))
                    return category;
            }
        }

        return null;
    }
}
