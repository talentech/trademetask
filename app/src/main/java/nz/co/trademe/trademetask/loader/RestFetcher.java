package nz.co.trademe.trademetask.loader;

import nz.co.trademe.trademetask.model.ModelBase;

/**
 * Type-safe REST service data fetcher
 */
public interface RestFetcher<D extends ModelBase> {
    RestResponse<D> fetch();
    void cancel();
}
