package nz.co.trademe.trademetask.model;

import java.util.List;

/**
 * Listing list.
 * Will be created by JSON converter.
 */
public class Listing extends ModelBase {

    public static class Item {
        private String ListingId;
        private String Title;
        private String PictureHref;

        public String getId() {
            return ListingId;
        }

        public String getTitle() {
            return Title;
        }

        public String getPictureHref() {
            return PictureHref;
        }
    }

    private List<Item> List;

    public List<Item> getItemList() {
        return this.List;
    }

    public boolean isItemListEmpty() {
        return this.List == null || this.List.size() == 0;
    }
}
