package nz.co.trademe.trademetask;


/**
 * Utility class used to access the component
 */
public class Injector {

    private Injector() {
    }

    static TradeMeTaskComponent tradeMeTaskComponent;

    static public TradeMeTaskComponent component() {
        return tradeMeTaskComponent;
    }

}
