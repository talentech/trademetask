package nz.co.trademe.trademetask.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nz.co.trademe.trademetask.model.Category;

/**
 * Data structure to represent the stack-like category list
 */
public class CategoryStack {

    // from root to current selected category
    // element(i) is parent of element(i+1)
    private final List<Category> categories = new ArrayList<>();

    public CategoryStack(Category rootCategory, List<String> categoryNumbers) {
        categories.add(rootCategory);

        // create the category list from number list
        if (categoryNumbers != null) {
            int size = categoryNumbers.size();
            if (size > 1) {
                Category category = rootCategory;
                for (int i = 1; i < size; i ++) {
                    Category subcategory = category.getSubcategory(categoryNumbers.get(i));
                    if (subcategory != null) {
                        category = subcategory;
                        categories.add(category);
                    }
                    else
                        break;
                }
            }
        }

    }

    public Category getCurrentCategory() {
        return categories.get(categories.size() - 1);
    }

    // new category can be parent, sibling or child of current category
    public void setCurrentCategory(String number) {

        int size = categories.size();

        // check if it is parent
        boolean found = false;
        Iterator<Category> iterator = categories.iterator();
        while (iterator.hasNext()) {
            if (found) {
                iterator.next();
                iterator.remove();
            }
            else if (iterator.next().getNumber().equals(number))
                found = true;
        }

        if (!found) {
            // check if it is child
            Category category = categories.get(size - 1).getSubcategory(number);

            if (category != null)
                categories.add(category);
            else if (size > 1) {
                // check if it is sibling
                category = categories.get(size - 2).getSubcategory(number);

                if (category != null)
                    categories.set(categories.size() - 1, category);
            }
        }
    }

    public void pop() {
        if (categories.size() > 1)
            categories.remove(categories.size() - 1);
    }

    public ArrayList<String> getCategoryNumbers() {
        ArrayList<String> categoryNames = new ArrayList<>(categories.size());
        for (Category category : categories)
            categoryNames.add(category.getNumber());

        return categoryNames;
    }

    private CategoryEntry createCategoryEntry(Category category,
                                              CategoryEntry.Type type,
                                              boolean isCurrent) {
        return new CategoryEntry(category.getName(), category.getNumber(), type,
                isCurrent, category.hasSubcategories());
    }

    /**
     * Determines what to display.
     *
     * The rules are:
     * 1. if current category (last item of categories) has subcategories, displays all its parents,
     * itself and all subcategories;
     * 2. if not, displays all its parents, itself and all its siblings
     *
     * @return List of CategoryEntry for CategoryAdapter to display
     */
    public List<CategoryEntry> getCateoryEntries() {
        List<CategoryEntry> categoryEntries = new ArrayList<>();

        int size = categories.size();

        // add root entry
        categoryEntries.add(createCategoryEntry(categories.get(0),
                CategoryEntry.Type.ROOT,
                size == 1));

        // add parent entries
        for (int i = 1; i < size - 1; i ++) {
            Category category = categories.get(i);
            categoryEntries.add(createCategoryEntry(category,
                    CategoryEntry.Type.PARENT,
                    false));
        }

        Category currentCategory = categories.get(size - 1);
        if (currentCategory.hasSubcategories()) {
            // add current entry
            if (size > 1)
                categoryEntries.add(createCategoryEntry(currentCategory,
                        CategoryEntry.Type.PARENT,
                        true));

            // add child entries
            for (Category category : currentCategory.getSubcategories())
                categoryEntries.add(createCategoryEntry(category,
                        CategoryEntry.Type.CHILD,
                        false));
        }
        else {
            Category parentCategory = categories.get(size - 2);

            // add current and sibling entries
            for (Category category : parentCategory.getSubcategories())
                categoryEntries.add(createCategoryEntry(category,
                        CategoryEntry.Type.CHILD,
                        category == currentCategory));
        }

        return categoryEntries;
    }

}
