package nz.co.trademe.trademetask;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

/**
 * MainActivity contains CategoryFragment and ListingFragment.
 * If screen width >= 600dp, they will be side by side;
 * else CategoryFragment will be in a left-gravity drawer
 */
public class MainActivity extends Activity {

    private DrawerLayout drawerLayout;
    private View categoryContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        categoryContainer = findViewById(R.id.category_container);

        // if drawer exists, set it to no more than 2/3 of screen width
        if (drawerLayout != null) {
            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            Point screenSize = new Point();
            display.getSize(screenSize);

            DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) categoryContainer.getLayoutParams();
            layoutParams.width = Math.min(screenSize.x * 2 / 3, layoutParams.width);
            categoryContainer.setLayoutParams(layoutParams);

            // open the drawer in first launch
            if (savedInstanceState == null)
                openDrawer();
        }

        // Create the CategoryFragment and add it to category_container
        FragmentManager fm = getFragmentManager();
        if (fm.findFragmentById(R.id.category_container) == null) {
            CategoryFragment categoryFragment = new CategoryFragment();
            fm.beginTransaction().add(R.id.category_container, categoryFragment).commit();
        }

        // Create the ListingFragment and add it to listing_container
        if (fm.findFragmentById(R.id.listing_container) == null) {
            ListingFragment listingFragment = new ListingFragment();
            fm.beginTransaction().add(R.id.listing_container, listingFragment).commit();
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if in drawer mode.
        if (drawerLayout != null)
            getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        // toggle drawer
        if (id == R.id.action_categories) {
            if (isDrawerOpen())
                closeDrawer();
            else
                openDrawer();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openDrawer() {
        if (drawerLayout != null)
            drawerLayout.openDrawer(categoryContainer);
    }

    private void closeDrawer() {
        if (drawerLayout != null)
            drawerLayout.closeDrawer(categoryContainer);
    }

    private boolean isDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(categoryContainer);
    }
}
