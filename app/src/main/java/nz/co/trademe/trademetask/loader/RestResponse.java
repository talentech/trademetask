package nz.co.trademe.trademetask.loader;

import nz.co.trademe.trademetask.model.ModelBase;

/**
 * Wrapper of model objects, returning from loader
 */
public class RestResponse<D extends ModelBase> {

    private String error;

    private D data;

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return data != null && data.isSuccess();
    }

    public String getErrorMessage() {
        if (data != null) {
            if (data.getErrorDescription() != null)
                return data.getErrorDescription();
            else if (!data.isSuccess() && data.getDescription() != null)
                return data.getDescription();
        }

        return error;
    }
}
