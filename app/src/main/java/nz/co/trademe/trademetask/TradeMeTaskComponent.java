package nz.co.trademe.trademetask;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Component that describes injected objects
 */
@Component(modules = TradeMeTaskModule.class)
@Singleton
public interface TradeMeTaskComponent {

    void inject(CategoryFragment categoryFragment);
    void inject(ListingFragment listingFragment);
}
