package nz.co.trademe.trademetask.model;

/**
 * Base class of model classes.
 * Response may contain these common fields.
 */
public class ModelBase {
    private boolean Success = true;
    private String Description;
    private String ErrorDescription;

    public boolean isSuccess() {
        return Success;
    }

    public String getDescription() {
        return Description;
    }

    public String getErrorDescription() {
        return ErrorDescription;
    }
}
