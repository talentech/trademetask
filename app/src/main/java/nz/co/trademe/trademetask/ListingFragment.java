package nz.co.trademe.trademetask;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import nz.co.trademe.trademetask.adapter.ListingAdapter;
import nz.co.trademe.trademetask.config.Config;
import nz.co.trademe.trademetask.event.SelectCategoryEvent;
import nz.co.trademe.trademetask.loader.RestLoader;
import nz.co.trademe.trademetask.model.Listing;
import nz.co.trademe.trademetask.loader.RestResponse;

/**
 * ListingFragment displays first 20 listings of a category.
 */
public class ListingFragment extends ListFragment  implements
        LoaderManager.LoaderCallbacks<RestResponse<Listing>>  {

    private static final String TAG = "TradeMeTask";
    private static final boolean DEBUG = false;


    // The Loader's id (this id is specific to the Activity/Fragment's LoaderManager)
    private static final int LOADER_ID = 1;

    private static final String KEY_CATEGORY_NUMBER = "KEY_CATEGORY_NUMBER";

    // a custom ArrayAdapter to bind category data to the ListView.
    private ListingAdapter adapter;

    // loader used to load listing data from server
    @Inject
    Provider<RestLoader<Listing>> listingLoader;

    // Otto event bus
    @Inject
    Bus bus;

    // number of the category that the displayed listings belong to. used to save state of the
    // fragment
    // if categoryNumber == null, it means no category is selected (first launching of the app, or
    // clicking root category in category stack)
    private String categoryNumber;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        if (savedInstanceState != null)
            categoryNumber = savedInstanceState.getString(KEY_CATEGORY_NUMBER);

        Injector.component().inject(this);
        bus.register(this);

        adapter = new ListingAdapter(getActivity());
        setListAdapter(adapter);

        if (categoryNumber != null)
            setListShown(false);
        else {
            setEmptyText(getActivity().getString(R.string.welcome));
            setListShownNoAnimation(true);
        }

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        if (categoryNumber != null)
            outState.putString(KEY_CATEGORY_NUMBER, categoryNumber);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public Loader<RestResponse<Listing>> onCreateLoader(int id, Bundle args) {
        if (DEBUG) Log.i(TAG, "+++ onCreateLoader() called! +++");

        return listingLoader.get();
    }

    @Override
    public void onLoadFinished(Loader<RestResponse<Listing>> loader,
                               RestResponse<Listing> restResponse) {
        if (DEBUG) Log.i(TAG, "+++ onLoadFinished() called! +++");

        if (categoryNumber == null) {
            setEmptyText(getActivity().getString(R.string.welcome));
            adapter.setData(null);
        }
        else if (restResponse != null && restResponse.isSuccess()) {
            setEmptyText(getActivity().getString(R.string.no_listings));
            adapter.setData(restResponse.getData().getItemList());
        }
        else {
            // no data, show error message
            setEmptyText(getActivity().getString(R.string.listing_load_error));
            adapter.setData(null);
        }

        if (isResumed())
            setListShown(true);
        else
            setListShownNoAnimation(true);
    }

    @Override
    public void onLoaderReset(Loader<RestResponse<Listing>> loader) {
        if (DEBUG) Log.i(TAG, "+++ onLoadReset() called! +++");

        adapter.setData(null);
    }


    private Map<String, String> createUrlParams(String categoryNumber) {
        Map<String, String> options = new HashMap<>();
        options.put("category", categoryNumber);
        options.put("rows", Config.FETCH_LISTING_ROWS);

        return options;
    }

    // update url parameters and force reloading data
    private void loadListing(String categoryNumber) {
        Loader<RestResponse<Listing>> loader = getLoaderManager().getLoader(LOADER_ID);

        if (loader != null) {
            RestLoader<Listing> restLoader = (RestLoader<Listing>) loader;

            Map<String, String> options = createUrlParams(categoryNumber);
            restLoader.setUrlParams(options);

            restLoader.onContentChanged();

            setListShown(false);
        }
    }

    // called when a category is selected
    @Subscribe
    public void selectCategory(SelectCategoryEvent event) {
        if (!event.isRoot()) {
            categoryNumber = event.getCategoryNumber();
            loadListing(categoryNumber);
        }
        else {
            categoryNumber = null;
            setEmptyText(getActivity().getString(R.string.welcome));
            adapter.setData(null);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_listing, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh_listing:
                if (categoryNumber != null)
                    loadListing(categoryNumber);
                return true;
        }
        return false;
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        super.onListItemClick(l, v, position, id);

        // TODO: open listing details Activity. this is trival so not implemented

    }


}
